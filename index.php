<?php
$gender = array("Nam", "Nữ");
$department = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">

    <!-- base style  -->
    <link rel="stylesheet" href="styles.css">

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <title>All Student</title>
</head>

<body>
<body>
    <div class="all-student">
        <div class="search-box">
            <form action="" method="get">
                <div class="input-box">
                <label for="department" class="text-label">Phân khoa</label>
                    <select name="department" id="department" class="select-field">
                        <option value=''>Chọn phân khoa</option>
                        <?php
                        foreach ($department as $key => $value) {
                            echo "<option value='$key'>$value</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="input-box">
                    <label for="" class="text-label">Từ khóa</label>
                    <input type="text" class="text-field" name="keyword">
                </div>
                <div class="btn">
                    <input type="submit" class="btn-submit" value="Tìm kiếm"></input>
                </div>
            </form>
        </div>
        <div class="student-count">
            <div class="student-found">
                <p><b>Số sinh viên tìm thấy: XXX</b></p>
            </div>
            <a href="create.php" class="btn-submit">Thêm</a>
        </div>
        <div class="student-list">
            <table class="student-table">
                <tr class="header">
                    <td style="width:20%"><b>No.</b></td>
                    <td style="width:50%"><b>Tên sinh viên</b></td>
                    <td style="width:30%"><b>Khoa</b></td>
                    <td style="text-align:center; width:10%"><b>Action</b></td>
                </tr>
                <tr class="student-item">
                    <td>1</td>
                    <td style="">Nguyễn Thị Hải Anh</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="btn-action">Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>2</td>
                    <td style="">Hải Anh Nguyễn Thị</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="btn-action" >Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>3</td>
                    <td style="">Thị Nguyễn Hải Anh</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="btn-action">Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>4</td>
                    <td style="">Anh Nguyễn Thị Hải</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="btn-action">Xóa</buton>
                        <buton class="btn-action">Sửa</buton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>


</html>

